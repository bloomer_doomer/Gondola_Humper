extends Node2D


func _ready():
	$AnimationPlayer.play("pop_up")

	match Globals.click_bonus:
		420:
			$Label.text = "Blaze it."
		69:
			$Label.text = "Nice."
		_:
			$Label.text = "+" + str(Globals.click_bonus)
