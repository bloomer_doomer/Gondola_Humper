extends Button



export(int) var price
export(int) var apm_increase = 0
export(int) var click_increase = 0
export(Texture) var upgradeImage

enum {APM_INCREASER, CLICK_INCREASER}

export var type = APM_INCREASER

var purchasing = false # are currently purchasing this upgrade

var previous_disabled_state = true


func _process(_delta):
	if previous_disabled_state != disabled and disabled == false:
		previous_disabled_state = false
		$AnimationPlayer.play("became_active")
		$AudioBecameActive.play()

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("ready")
	$PriceTag.text = $PriceTag.text + " " + str(price)
	
	var below_image = Sprite.new()
	below_image.texture = upgradeImage
	below_image.position.y = rect_size.y / 2 - 12
	below_image.position.x = $Icon.texture.get_size().x / 4 - below_image.texture.get_size().x / 4 + 16
	if type == APM_INCREASER:
		var increase_tag = Label.new()
		increase_tag.text = "+%s" % str(apm_increase)
		increase_tag.rect_position += Vector2(-24, 24)
		below_image.add_child(increase_tag)
		
	add_child(below_image)

	$Icon.texture = upgradeImage
	$Icon.position.y = rect_size.y / 2 - 12
	$Icon.position.x = $Icon.texture.get_size().x / 4 - $Icon.texture.get_size().x / 4 + 16
	
	if type == CLICK_INCREASER:
		text = "+%s ClickBonus" % click_increase


func press_juice():
	$AudioUnique.play()
	$AudioClick0.play()
	$AudioClick1.play()
	$AnimationPlayer.play("pressed")
	$AudioAccept.play()

func deny():
	$AudioDeny.play()


func _on_AnimationPlayer_animation_finished(_anim_name):
	$AnimationPlayer.play("ready")



func _on_BaseButton_mouse_entered():
	Globals.mouse_on_shop = true
	grab_focus()
	grab_click_focus()


func _on_BaseButton_mouse_exited():
	Globals.mouse_on_shop = false
