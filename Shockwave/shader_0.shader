shader_type canvas_item;
render_mode unshaded;


uniform vec2 center = vec2(.5, .5);
uniform float force : hint_range(-2, 2) = 1;
uniform float size : hint_range(0, 4) = .2;
uniform float thickness : hint_range(0, 1) = .1;
uniform float feather : hint_range(0, 1.3) = .1;

uniform bool apply = true;
uniform float chromatic_abberration_amount : hint_range(0, 1) = .2;

void fragment() {

	// start chromatic abberration
	vec4 texture_color = texture(TEXTURE, UV);
	vec4 col = texture_color;
	if (apply){
		float ratio = SCREEN_PIXEL_SIZE.x / SCREEN_PIXEL_SIZE.y;
		vec2 scaled_uv = (SCREEN_UV - vec2(0.5, 0.0)) / vec2(ratio, 1.0) + vec2(0.5, 0.0);
		float thickness_rel = size * thickness;
		float mask = (1.0 -  smoothstep(size - feather, size, length(scaled_uv - center))) *
					smoothstep(size - thickness_rel - feather, size - thickness_rel, length(scaled_uv - center));
		
		vec2 disp = normalize(scaled_uv - center) * force * mask;
		vec4 invis = vec4(0.0);
		float adjusted_amount = (chromatic_abberration_amount) * mask;
		col.r = texture(SCREEN_TEXTURE, SCREEN_UV - disp*1.45).r;
		col.g = texture(SCREEN_TEXTURE, SCREEN_UV - disp*0.75).g;
		col.b = texture(SCREEN_TEXTURE, SCREEN_UV - disp*1.0).b;
		COLOR = col;
	}
	else{
		COLOR = texture(SCREEN_TEXTURE, SCREEN_UV);
	}
	// end chromatic abberration

	//COLOR = texture(SCREEN_TEXTURE, SCREEN_UV);

	// COLOR.rgb = vec3(mask);
}