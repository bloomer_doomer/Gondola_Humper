extends TextureRect


func _ready():
	var effectPos = get_node("/root/gondola/SplashPos")
	var x_pos = effectPos.position.x
	var y_pos = effectPos.position.y
	var viewport_rect = get_viewport_rect().size
	material.set_shader_param("Center", Vector2(x_pos / viewport_rect.x,\
												y_pos / viewport_rect.y ))
