extends Node2D


onready var anim = $AnimationPlayer
onready var animTree = $AnimationTree
onready var animState = animTree.get("parameters/playback")
onready var scoreLabel = $HUD/VBoxContainer/ScoreLabel
onready var shockwavePlayer = $Shockwave/ShockwaveAnimation
onready var inputTimer = $InputBuffer
onready var splashEffect : PackedScene = preload("res://Splash.tscn")
onready var clickPopup : PackedScene = preload("res://ClickNotificationImage.tscn")


var previous_score = Globals.score
var apm_last_time



func _ready():
	anim.play("reset")
	apm_last_time = now()
	updateClickBonusText()

func _process(_delta):
	if Input.is_action_just_pressed("exit"):
		get_tree().quit()
	if now() - apm_last_time > Globals.apm_interval:
		var amount = (now() - apm_last_time) / Globals.apm_interval
		increase_score(amount)
		apm_last_time = now()
	if not climax_reached():
		if Input.is_action_just_pressed("hump"):
			if not Globals.mouse_on_shop:
				if anim.current_animation != "hump_down" and\
				not anim.is_playing() and\
				not shockwavePlayer.is_playing():
					hump()
		elif Input.is_action_just_released("hump") and\
		not shockwavePlayer.is_playing():
#			$Chromatic_aberration.material.set_shader_param("shader_param/apply", false)
			animState.travel("hump_up")
	update_text()
	update_buttons()

func passed_arousal_threshold(x):
	var p = previous_score
	var points_til_next_threshold = x - p % x 
	if Globals.score - p > points_til_next_threshold:
		return true
	else:
		return false

func increase_score(amount : int = 1) -> void:
	Globals.score += amount
	if not Globals.climaxed:
		if climax_reached():
			climax()
		elif passed_arousal_threshold(10000) and not $CelebrationSound2.playing:
			$CelebrationSound2.play()
		elif passed_arousal_threshold(1000) and not $CelebrationSound1.playing:
			$CelebrationSound1.play()
		elif passed_arousal_threshold(100) and not $CelebrationSound0.playing:
			$CelebrationSound0.play()
	previous_score = Globals.score

func hump():
	animState.travel("hump_down")
	increase_score(Globals.click_bonus)
	splash(.2)
	click_effect()
	update_text()

func try_purchase_upgrade(upgrade: Node) -> bool:
	"""Returns true on successful purchase, otherwise returns false."""
	if Globals.score >= upgrade.price and not Globals.climaxed:
		purchase_upgrade(upgrade)
		return true
	else:
		upgrade.deny()
		return false

func purchase_upgrade(upgrade):
	upgrade.press_juice()
	Globals.score -= upgrade.price
	animState.travel("hump_down")
	shockwavePlayer.play("shockwave")
	# wait for the time of the shockwave animation (12/24 frames; .5 seconds)
	if not climax_reached():
		yield(get_tree().create_timer(.5), "timeout")
	animState.travel("hump_up")	
	match upgrade.type:
		upgrade.APM_INCREASER:	
			update_apm(upgrade.apm_increase)
		upgrade.CLICK_INCREASER:
			update_click_bonus(upgrade.click_increase)
	update_text()

func update_click_bonus(click_increase):
	Globals.click_bonus += click_increase
	updateClickBonusText()

func update_apm(amount = 0):
	apm_last_time = now()
	Globals.apm += amount
	update_apm_interval()
	print("new apm: %s" % Globals.apm)

func update_apm_interval():
	if Globals.apm != 0:
		Globals.apm_interval = 60000.0 / Globals.apm
		print("new apm interval: %smsecs" % Globals.apm_interval)

func update_text():
	_update_apm_text()
	_update_score_text()

func _update_apm_text():
	$HUD/VBoxContainer/APMVbox/APMNumber.text = str(Globals.apm)

func _update_score_text():
	scoreLabel.text = "Arousal:\n%s" % Globals.score
	
func updateClickBonusText():
	$HUD/RightVBox/ClickBonus.text = "Click Bonus:\n%s" % Globals.click_bonus

func update_buttons():
	for child in $HUD/VBoxContainer/Shop/VBoxContainer.get_children():
		if Globals.score < child.price:
			child.disabled = true
		else:
			child.disabled = false
		# also update the plus one button
	for child in $HUD/RightVBox/ClickUpgrades/.get_children():
		if Globals.score < child.price:
			child.disabled = true
		else:
			child.disabled = false

func now():
	return OS.get_ticks_msec()

func climax_reached():
	return Globals.score >= Globals.climax_threshold

func climax():
	Globals.climaxed = true
	print(Globals.climaxed)
	animState.travel("climax")


func splash(scale: float) -> void:
	var splash = splashEffect.instance()
	splash.scale = Vector2(scale, scale)
	splash.position = $SplashPos.position
	add_child_below_node($Sprite, splash)
	splash.emitting = true

func click_effect():
	var click_effect = clickPopup.instance()
	click_effect.position = get_global_mouse_position()
	add_child(click_effect)

func _on_ClickUpgrade01_pressed():
# warning-ignore:return_value_discarded
	try_purchase_upgrade($HUD/RightVBox/ClickUpgrades/ClickUpgrade01)


func _on_ClickUpgrade02_pressed():
# warning-ignore:return_value_discarded
	try_purchase_upgrade($HUD/RightVBox/ClickUpgrades/ClickUpgrade02)

func _on_ClickUpgrade03_pressed():
# warning-ignore:return_value_discarded
	try_purchase_upgrade($HUD/RightVBox/ClickUpgrades/ClickUpgrade03)

func _on_Aphrotisiac_basic_pressed():
	var upgrade = $HUD/VBoxContainer/Shop/VBoxContainer/Aphrotisiac_basic
# warning-ignore:return_value_discarded
	try_purchase_upgrade(upgrade)

func _on_Aphrotisiac_strong_pressed():
	var upgrade = $HUD/VBoxContainer/Shop/VBoxContainer/Aphrotisiac_strong
# warning-ignore:return_value_discarded
	try_purchase_upgrade(upgrade)

func _on_Aphrotisiac_viscous_pressed():
	var upgrade = $HUD/VBoxContainer/Shop/VBoxContainer/Aphrotisiac_viscous
# warning-ignore:return_value_discarded
	try_purchase_upgrade(upgrade)

func exit():
	get_tree().quit()

