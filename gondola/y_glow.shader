shader_type canvas_item;

uniform bool apply : hint_bool = true;


void fragment(){
	vec4 tex_col = texture(TEXTURE, UV);
	tex_col.rgb /= vec3((.8 - UV.y));
	COLOR = tex_col;

}