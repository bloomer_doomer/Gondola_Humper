shader_type canvas_item;
uniform float caa_h : hint_range(-1.0,1.0) = .004; // chromatic aberration amount
uniform float jitter_h : hint_range(0.0,1.0) = .001; // chromatic aberration amount
uniform float caa_v : hint_range(-1.0,1.0) = .0; // chromatic aberration amount
uniform float jitter_v : hint_range(0.0,1.0) = .0; // chromatic aberration amount
uniform bool apply : hint_bool = true;

void fragment(){
	vec4 t_col = texture(SCREEN_TEXTURE, SCREEN_UV);
	if (apply){
		t_col.r = texture(SCREEN_TEXTURE, vec2(SCREEN_UV.x + caa_h, SCREEN_UV.y - caa_v)).r;
		t_col.g = texture(SCREEN_TEXTURE, SCREEN_UV).g;
		t_col.b = texture(SCREEN_TEXTURE, vec2(SCREEN_UV.x - caa_h, SCREEN_UV.y + caa_v)).b;
	}
	COLOR = t_col;
}
